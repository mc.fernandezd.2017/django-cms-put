from django.urls import path
from . import views

urlpatterns = [
    path("<llave>", views.get_content, name="get_content"),
    path("", views.index, name="index"),
]
