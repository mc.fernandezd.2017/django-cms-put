# from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido


# Create your views here.

formulario = """
<br><br>
<form action="" method="POST">
  Introduce el (nuevo) contenido para esta página: 
  <input type="text" name="valor">
  <input type="submit" value="Enviar">
</form>
"""


def index(request):
    return HttpResponse("<h1>Este es un CMS con POST</h1>")


@csrf_exempt  # Me salto mecanismo de seguridad
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
        try:
            contenido = Contenido.objects.get(clave=llave)
            contenido.valor = valor
        except Contenido.DoesNotExist:
            contenido = Contenido(clave=llave, valor=valor)
        contenido.save()

    if request.method == "POST":
        valor = request.POST['valor']
        try:
            contenido = Contenido.objects.get(clave=llave)
            contenido.valor = valor
        except Contenido.DoesNotExist:
            contenido = Contenido(clave=llave, valor=valor)
        contenido.save()

    c = get_object_or_404(Contenido, clave=llave)
    return HttpResponse(c.valor + formulario)
